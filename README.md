# Beaver

Python client for being able to send valid positions to Wolf.

## Dependencies

* Python 3.6+
* OpenSSL

## Install

* create a virtualenv
* `pip install -r requirements.txt`

## Usage

1. `./beaver.py generate_device`
2. `./beaver.py register_key`
3. `./beaver.py check_device_registration`
4. `./beaver.py send_position`

Run `./beaver.py --help` for more complete documentation.
