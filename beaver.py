#! /usr/bin/env python
import hashlib
import os
import random
import secrets
import socket
from datetime import datetime, timezone
from http import HTTPStatus

import jwt
import requests
from cryptography.hazmat.primitives import serialization
from minicli import cli, run
from OpenSSL import crypto

MOOSE_URL = 'http://127.0.0.1:5000/'
WOLF_URL = 'http://127.0.0.1:8080/'
UDP_HOST = os.environ.get('UDP_HOST', 'localhost')
UDP_PORT = 5005
ALGORITHM = 'RS512'


@cli
def generate_device(bits=1024):
    """First step:
    Generate registration number and private/public keys for a device

    :bits: The number of bits for the generated keys.
    """
    random_number = ''.join(str(secrets.randbelow(10)) for i in range(12))
    registration = f'BEAVER{random_number}FR'
    print(f'Registration number: {registration}\n')
    pkey = crypto.PKey()
    pkey.generate_key(crypto.TYPE_RSA, bits)
    private_key = pkey.to_cryptography_key()
    private_key_bytes = private_key.private_bytes(
        serialization.Encoding.PEM,
        serialization.PrivateFormat.TraditionalOpenSSL,
        serialization.NoEncryption())
    print(private_key_bytes.decode())
    public_key = private_key.public_key()
    public_key_bytes = public_key.public_bytes(
        serialization.Encoding.PEM,
        serialization.PublicFormat.SubjectPublicKeyInfo)
    print(public_key_bytes.decode())
    print('✓ All went fine, keep these informations for next steps.\n')
    print('{:>64}'.format('☞ Next step: register_key'))


@cli
def register_key():
    """Second step:
    Register a registration number and public key against Wolf.
    """
    registration = input('Registration number: ')
    sentinel = ''  # Ends when this string is seen.
    public_key = input('Public key (empty line to finish): ')
    public_key = '\n'.join([public_key] + list(iter(input, sentinel)))
    print(f'Adding `{registration}` device with key:\n{public_key}')
    try:
        response = requests.put(
            f'{WOLF_URL}device/{registration}',
            data=public_key)
    except requests.ConnectionError:
        print(f'✘ Error: connection refused to {WOLF_URL}.')
        return
    if response.status_code == HTTPStatus.CREATED:
        print(f'✓ Key successfully registered on {WOLF_URL}.\n')
        print('{:>64}'.format('☞ Next step: check_device_registration'))
    else:
        print(f'✘ Error: {response.status_code} {response.text}')


@cli
def check_device_registration():
    """Third step:
    Check if the device is registered against Moose.
    """
    registration = input('Registration number: ')
    print(f'Checking if `{registration}` is already registered…')
    response = requests.head(f'{MOOSE_URL}device/{registration}')
    if response.status_code == HTTPStatus.NO_CONTENT:
        print('✓ This device is registered! You are good to go.')
        print('{:>64}'.format('☞ Next step: send_position'))
    else:
        print('✘ This device is NOT registered, go register it and re-check!')


@cli
def send_position():
    """Fourth step:
    Send a signed position to Wolf.
    """
    bbox = [-4.99, 41.34, 9.69, 51.04]
    registration = input('Registration number: ')
    sentinel = ''  # Ends when this string is seen.
    private_key = input('Private key (empty line to finish): ')
    private_key = '\n'.join([private_key] + list(iter(input, sentinel)))
    print(f'Sending new position for `{registration}`')
    timestamp = datetime.now(timezone.utc).isoformat()
    elevation = secrets.randbelow(150)
    lat = random.uniform(float(bbox[0]), float(bbox[2]))
    lon = random.uniform(float(bbox[1]), float(bbox[3]))
    data = {
        'registration': registration,
        'time': timestamp,
        'coordinates': [lat, lon, elevation]
    }
    data = jwt.encode(data, private_key, algorithm=ALGORITHM)
    report_hash = hashlib.md5(data).hexdigest()
    print(f'Report hash: {report_hash} (useful to debug)')
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(data, (UDP_HOST, UDP_PORT))
    received = str(sock.recv(1024), 'utf-8')
    print(f'✓ Received from {UDP_HOST}: {received:.8}')


if __name__ == '__main__':
    run()
